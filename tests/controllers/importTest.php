<?php 
use PHPUnit\Framework\TestCase;
	class importTest extends TestCase {
		
		
		public function setUp()
		{
			
			parent::tearDown();
			parent::setUp();
			// Set the tested controller
			$this->CI = set_controller('import');

			$this->CI->load->model('m_monitor', 'm');
			// $this->CI = set_model('m_monitor');
			$this->CI->load->library('encrypt');
			// $this->CI = set_library('phpexcel/Classes/PHPExcel');
			// $this->CI = set_library('session');
			// $this->CI = set_helper('path');
			echo "\n Controller Test\n";
			$this->m=$this->CI->m;
			
		}
		

		function testRenaksi_xls()
		{
			// new object PHPHexcel
			$monitor_id_encode = $this->CI->encrypt->encode(2);
			$this->CI->renaksi_xls_test($monitor_id_encode);
			$renaksi = $this->m->export_data_renaksi_xls(2,0,0,0);
			$this->assertContains("5",$renaksi->result()[5]->uc_realisasi_fisik);
	}
}
