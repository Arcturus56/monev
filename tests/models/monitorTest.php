<?php 
use PHPUnit\Framework\TestCase;
	class m_monitor_test extends TestCase {
		
		public $monitor_id_encode;
	
		public function __construct()
	{
		parent::__construct();
	}
		public function setUp()
		{
			parent::setUp();
			// $this->CI = set_controller('import');

			$this->CI =& get_instance();
			$this->CI->load->model('m_monitor','m');
			
			$this->CI->load->library('encrypt');
			
			
			
			$this->m=$this->CI->m;
			
			
		}
		function test_get_monitor_type(){
			$result = $this->m->get_monitor_type();
			$this->assertContains("Kinerja Program (F8K)",$result);
		}
		function test_get_prioritas_list(){
			$this->monitor_id_encode = $monitor_id_encode = $this->CI->encrypt->encode(2);
			$expected_result = array(
				"prioritas_id"=> "3",
				"prioritas_name"=> "Kedaulatan",
				"prioritas_serial"=> "1",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt"
			);
			$result = $this->m->get_prioritas_list($this->monitor_id_encode);
			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
		function test_get_prioritas_list_count(){
			$this->monitor_id_encode = $monitor_id_encode = $this->CI->encrypt->encode(2);
			$expected_result = array(
				"sub_program_id"=> "3",
				"sub_program_name"=> "tebang",
				"sub_program_serial"=> "2",
				"program_id"=> "3",
				"program_name"=> "Program pengelolaan ruang laut",
				"program_serial"=> "1",
				"prioritas_id"=> "4",
				"prioritas_name"=> "Keberlanjutan",
				"prioritas_serial"=> "2",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt",
				"sub_program_percent"=>"21.22"
			);
			$result = $this->m->get_prioritas_list_count($this->monitor_id_encode);

			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
		function test_get_program_list(){
			$prioritas_id_encode = $prioritas_id_encode = $this->CI->encrypt->encode(4);
			$expected_result = array(
				"program_id"=> "3",
				"program_name"=> "Program pengelolaan ruang laut",
				"program_serial"=> "1",
				"prioritas_name"=> "Keberlanjutan",
				"prioritas_serial"=> "2",
				"prioritas_id"=> "4",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt",
				"program_percent"=> "21.22"
			);
			$result = $this->m->get_program_list($prioritas_id_encode);
			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
		function test_get_program_list_browse(){
			$prioritas_id_encode = $prioritas_id_encode = $this->CI->encrypt->encode(4);
			$expected_result = array(
				"program_id"=> "3",
				"program_name"=> "Program pengelolaan ruang laut",
				"program_serial"=> "1",
				"prioritas_name"=> "Keberlanjutan",
				"prioritas_serial"=> "2",
				"prioritas_id"=> "4",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt",
			);
			$result = $this->m->get_program_list_browse($prioritas_id_encode);
			
			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
		function test_get_sub_program_list(){
			$program_id_encode = $program_id_encode = $this->CI->encrypt->encode(3);
			$expected_result = array(
				"program_id"=> "3",
				"program_name"=> "Program pengelolaan ruang laut",
				"program_serial"=> "1",
				"prioritas_name"=> "Keberlanjutan",
				"prioritas_serial"=> "2",
				"prioritas_id"=> "4",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt",
				"sub_program_id"=> "1",
				"sub_program_name"=> "tanams",
				"sub_program_serial"=> " 1",
			);
			$result = $this->m->get_sub_program_list($program_id_encode);			
			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
		function test_get_sub_program_list_count(){
			$program_id_encode = $program_id_encode = $this->CI->encrypt->encode(3);
			$expected_result = array(
				"program_id"=> "3",
				"program_name"=> "Program pengelolaan ruang laut",
				"program_serial"=> "1",
				"prioritas_name"=> "Keberlanjutan",
				"prioritas_serial"=> "2",
				"prioritas_id"=> "4",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt",
				"sub_program_id"=> "3",
				"sub_program_name"=> "tebang",
				"sub_program_serial"=> " 2",
				"sub_program_percent"=> " 21.22",
			);
			$result = $this->m->get_sub_program_list_count($program_id_encode);
			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
		function test_get_renaksi_list_count(){
			$sub_program_id_encode = $sub_program_id_encode = $this->CI->encrypt->encode(3);
			$expected_result = array(
				"program_id"=> "3",
				"program_name"=> "Program pengelolaan ruang laut",
				"program_serial"=> "1",
				"prioritas_name"=> "Keberlanjutan",
				"prioritas_serial"=> "2",
				"prioritas_id"=> "4",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt",
				"sub_program_id"=> "3",
				"sub_program_name"=> "tebang",
				"sub_program_serial"=> "2",
				"renaksi_id"=> "1",
				"renaksi_name"=> "Penanaman mangrove",
				"renaksi_serial"=> "1",
				"renaksi_percent"=> "45.00",
			);
			$result = $this->m->get_renaksi_list_count($sub_program_id_encode);
			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
		function test_get_renaksi_list(){
			$sub_program_id_encode = $sub_program_id_encode = $this->CI->encrypt->encode(3);
			$expected_result = array(
				"program_id"=> "3",
				"program_name"=> "Program pengelolaan ruang laut",
				"program_serial"=> "1",
				"prioritas_name"=> "Keberlanjutan",
				"prioritas_serial"=> "2",
				"prioritas_id"=> "4",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt",
				"sub_program_id"=> "3",
				"sub_program_name"=> "tebang",
				"sub_program_serial"=> "2",
				"renaksi_id"=> "1",
				"renaksi_name"=> "Penanaman mangrove",
				"renaksi_serial"=> "1",
			);
			$result = $this->m->get_renaksi_list($sub_program_id_encode);
			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
		function test_export_data_renaksi_xls(){

			$expected_result = array(
				"monitor_type"=> "1",
				"monitor_observer"=> "4",
				"monitor_verifikator"=> "4,10",
				"monitor_status"=> "2",
				"prioritas_is_active"=> "1",
				"program_is_active"=> "1",
				"sub_program_is_active"=> "1",
				"renaksi_domain_id"=> null,
				"renaksi_is_active"=> "1",
				"kriteria_id"=> "1",
				"kriteria_name"=> "Pembelian pohon",
				"kriteria_penanggung_jawab"=> "91",
				"kriteria_instansi_terkait"=> "",
				"kriteria_is_active"=> "1",
				"ukuran_id"=> "42",
				"ukuran_name"=> "a",
				"ukuran_type"=> "1",
				"ukuran_jumlah_anggaran"=> "0",
				"ukuran_finish"=> "0",
				"ukuran_finish_on"=> "16",
				"ukuran_is_active"=> "1",
				"uc_id"=> "15",
				"uc_name"=> "b",
				"uc_capaian"=> "90",
				"uc_keterangan"=> null,
				"uc_status"=> "2",
				"uc_target_fisik"=> null,
				"uc_target_keuangan"=> null,
				"uc_realisasi_fisik"=> null,
				"uc_realisasi_keuangan"=> null,
				"mc_id"=> "1",
				"mc_year"=> "19",
				"mc_month"=> "3",
				"mc_week"=> "4",
				"mc_status"=> "2",
				"name"=> "Kementerian Lingkungan Hidup dan Kehutanan",
				"instansi_induk"=> "Pemerintah Republik Indonesia",
				"program_id"=> "3",
				"program_name"=> "Program pengelolaan ruang laut",
				"program_serial"=> "1",
				"prioritas_name"=> "Keberlanjutan",
				"prioritas_serial"=> "2",
				"prioritas_id"=> "4",
				"monitor_id"=>"2",
				"monitor_name"=>"Program unggulan",
				"monitor_code"=>"pt",
				"sub_program_id"=> "3",
				"sub_program_name"=> "tebang",
				"sub_program_serial"=> "2",
				"renaksi_id"=> "1",
				"renaksi_name"=> "Penanaman mangrove",
				"renaksi_serial"=> "1",
			);
			$result = $this->m->export_data_renaksi_xls(2,0,0,0);
			$this->assertEquals($expected_result,(array)$result->result()[0]);
		}
}