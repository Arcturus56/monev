<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class import extends CI_Controller {
		
		function __construct()
		{
			parent::__construct();
			$this->general->checkLogin();
			$this->load->library('pdf');
			$this->load->library("phpexcel/Classes/PHPExcel");	
			$this->load->model('m_monitor');
			$this->load->helper('path');
			$this->ci =& get_instance();
			backButtonHandle();
		}
		function import_renaksi($monitor_id_encode)
		{
			redirect('import/renaksi_xls/'.$monitor_id_encode);
		}
		
		

		function renaksi_xls($monitor_id_encode)
		{
			// new object PHPHexcel
			libxml_disable_entity_loader(false);
			$inputFileName = getcwd().'tests\files\laporan.xlsx';
			
			$periode = 0;
			$prioritas = 0;
			$penanggung_jawab = 0;
			$monitor_id_decode = $this->encrypt->decode($monitor_id_encode);
			$monitor = $this->m_monitor->export_data_monitor($monitor_id_decode);
			$detil_monitor = $monitor->row();

			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$renaksi = $this->m_monitor->export_data_renaksi_xls($monitor_id_decode,$periode,$prioritas,$penanggung_jawab);
			for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
				//Sesuaikan sama nama kolom tabel di database     
				// var_dump($renaksi->result()[$row - 2]);                           
				$data_update = array(
                    "target_keuangan"=> $rowData[0][13],
                    "realisasi_keuangan"=> $rowData[0][21],
                    "target_fisik"=> $rowData[0][14],
                    "realisasi_fisik"=> $rowData[0][22],
					"capaian"=> $rowData[0][19],
					'status'	 		=> 2
				);
				
				//sesuaikan nama dengan nama tabel
				$ukuran_id = $renaksi->result()[$row - 2]->ukuran_id;
				$checkpoint_id = $renaksi->result()[$row - 2]->mc_id;
				$update = $this->m_monitor->lapor_xls($ukuran_id, $checkpoint_id, $data_update);
				$monitor_id = $this->m_monitor->get_monitor_ukuran_checkpoint_id($ukuran_id, $checkpoint_id)->result()[0]->id;
				if($renaksi->result()[$row - 2]->ukuran_type == 2){
					$data_insert_logs = array (
						'timestamp' 		=> date('Y-m-d h:i:s'),
						'monitor_ukuran_checkpoint_id'	=> $monitor_id,
						'capaian'	 		=> $rowData[0][19],
						'realisasi_keuangan'=> $rowData[0][21],
						'realisasi_fisik'	=> $rowData[0][22],
						'keterangan'		=> $rowData[0][25],
						'log_ref_id'	 	=> 1,
						'user_id'	 		=> $this->session->userdata('user_id')
						);
				}else{
					$data_insert_logs = array (
						'timestamp' 		=> date('Y-m-d h:i:s'),
						'monitor_ukuran_checkpoint_id'	=> $monitor_id,
						'capaian'	 		=> $rowData[0][19],
						'keterangan'		=> $rowData[0][25],
						'log_ref_id'	 	=> 1,
						'user_id'	 		=> $this->session->userdata('user_id')
						);
				}
				
				$this->m_monitor->logs_add($data_insert_logs);
				
            }
			unlink($inputFileName) or die("Couldn't delete file");
			redirect('monitor');
		 }
		 function renaksi_xls_test($monitor_id_encode)
		{
			// new object PHPHexcel
			libxml_disable_entity_loader(false);
			$inputFileName = getcwd().'\tests\files\laporan.xlsx';
			
			$periode = 0;
			$prioritas = 0;
			$penanggung_jawab = 0;
			$monitor_id_decode = $this->encrypt->decode($monitor_id_encode);
			$monitor = $this->m_monitor->export_data_monitor($monitor_id_decode);
			$detil_monitor = $monitor->row();

			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$renaksi = $this->m_monitor->export_data_renaksi_xls($monitor_id_decode,$periode,$prioritas,$penanggung_jawab);
			for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
				//Sesuaikan sama nama kolom tabel di database     
				// var_dump($renaksi->result()[$row - 2]);                           
				$data_update = array(
                    "target_keuangan"=> $rowData[0][13],
                    "realisasi_keuangan"=> $rowData[0][21],
                    "target_fisik"=> $rowData[0][14],
                    "realisasi_fisik"=> $rowData[0][22],
					"capaian"=> $rowData[0][19],
					'status'	 		=> 2
				);
				
				//sesuaikan nama dengan nama tabel
				$ukuran_id = $renaksi->result()[$row - 2]->ukuran_id;
				$checkpoint_id = $renaksi->result()[$row - 2]->mc_id;
				$update = $this->m_monitor->lapor_xls($ukuran_id, $checkpoint_id, $data_update);
				$monitor_id = $this->m_monitor->get_monitor_ukuran_checkpoint_id($ukuran_id, $checkpoint_id)->result()[0]->id;
				if($renaksi->result()[$row - 2]->ukuran_type == 2){
					$data_insert_logs = array (
						'timestamp' 		=> date('Y-m-d h:i:s'),
						'monitor_ukuran_checkpoint_id'	=> $monitor_id,
						'capaian'	 		=> $rowData[0][19],
						'realisasi_keuangan'=> $rowData[0][21],
						'realisasi_fisik'	=> $rowData[0][22],
						'keterangan'		=> $rowData[0][25],
						'log_ref_id'	 	=> 1,
						'user_id'	 		=> $this->session->userdata('user_id')
						);
				}else{
					$data_insert_logs = array (
						'timestamp' 		=> date('Y-m-d h:i:s'),
						'monitor_ukuran_checkpoint_id'	=> $monitor_id,
						'capaian'	 		=> $rowData[0][19],
						'keterangan'		=> $rowData[0][25],
						'log_ref_id'	 	=> 1,
						'user_id'	 		=> $this->session->userdata('user_id')
						);
				}
				
				$this->m_monitor->logs_add($data_insert_logs);
				
            }
			
			redirect('monitor');
         }
	}
