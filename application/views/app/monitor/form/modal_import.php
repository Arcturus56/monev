<script> 
    // wait for the DOM to be loaded 
    jQuery(document).ready(function() {
		
		$('#lapor').ajaxForm(function(data) {
			if(data == 'success') {
				location.reload();
				} else {
				var container = $('#myModal');
				container.html(data);
			}
		}); 
		$(".textarea").wysihtml5();
	}); 
	function replaceChars(entry) {
		out = "."; // replace this
		add = ""; // with this
		temp = "" + entry; // temporary holder

		while (temp.indexOf(out)>-1) {
		pos= temp.indexOf(out);
		temp = "" + (temp.substring(0, pos) + add + 
		temp.substring((pos + out.length), temp.length));
		}
	}

	function trimNumber(s) {
	  while (s.substr(0,1) == '0' && s.length>1) { s = s.substr(1,9999); }
	  while (s.substr(0,1) == '.' && s.length>1) { s = s.substr(1,9999); }
	  return s;
	}

	function formatangka(objek) {
		a = objek.value;
		b = a.replace(/[^\d]/g,"");
		c = "";
		panjang = b.length;
		j = 0;
		for (i = panjang; i > 0; i--) {
		j = j + 1;
		if (((j % 3) == 1) && (j != 1)) {
		c = b.substr(i-1,1) + "." + c;
		} else {
		c = b.substr(i-1,1) + c;
		}
		}
		objek.value = trimNumber(c);
	}
</script>  
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title"><b>Import</b></h4>
		</div>
		<div class="modal-body">
			<?php 
				if(validation_errors()){
					echo "
					<div class='form-group'>
					<label class='control-label col-md-12'><div class='alert alert-danger'>
					<button class=close data-close='alert'></button>
					<div class='text-center'>".validation_errors()."</div>
					</div></label>
					</div>";
				}
			?>
			<?=form_open('import/import_renaksi/'.$monitor_id.'', '', array('class'=>'form-horizontal'));?>
			<div class="form-group">
				<label class="control-label col-md-3">Data Dukung
				<br>
				 Export data terlebih dahulu
				<br>
				 Format penamaan laporan.xlsx
				<br>
				</label>
				<div class="col-md-9">
					<div class="input-icon right">
						<input type="file" name="userfile" id="upload_btn" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3">
				</label>
				<div class="form-group col-md-9">
					<div class="input-icon right">
						<div id="files">
							<div id="filename"></div>
							<div>
								<div id="progress" style="width: 75%; display: inline-block;">
									<div class="bar" style="width: 0%; height: 10px; background: green;"></div>
								</div>
								<div id="percent" style="width: 20%; display: inline-block;"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			<div class="modal-footer">
				<?=form_submit('import', 'Import', 'class="btn blue"'); ?>
			</div>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
<script type='text/javascript' >
$(function () {
    $('#upload_btn').fileupload({
		dataType: 'json',
		singleFileUploads: true,
		url: '<?php echo base_url('upload/laporan');?>',
		progressall: function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#progress .bar').css(
				'width',
				progress + '%'
			);
			document.getElementById('percent').innerHTML = progress + '%';
		},
		add: function (e, data) {
			console.log('adding');
			console.log(data);
            data.context = $('<p/>').text(data.files[0].name).appendTo('#filename');
            data.submit();
        },
        done: function (e, data) {
			console.log('DONE');
			console.log(data);
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
            });
		}
    });
});
</script>